<?php

use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('index');

//Rutas para login
Route::post('/login', [
    'as' => 'login', 
    'uses' => 'Auth\LoginController@login'
]);

//Ruta para el dashboard
Route::get('/dashboard', 'DashboardController@index')->name('dash.historial');

//Ruta para crear temas
Route::get('/generar', 'DashboardController@crear')->name('dash.crear');
Route::post('/agregar', [
    'as' => 'agregar', 
    'uses' => 'DashboardController@agregar'
]);

//Ruta para ver las graficas
Route::get('/grafica/{id}', 'DashboardController@graficas')->name('graficas');

/*Route::get('/usuarios', function(){
    $usuarios = User::where('nbr_usuario','MM180363')->get();
    dd($usuarios);
});*/
