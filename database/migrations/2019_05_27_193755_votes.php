<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Votes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_carnet')->unsigned();
            $table->integer('id_tema')->unsigned();
            $table->integer('num_valor');
            $table->timestamps();
    
            //Llave foranea
            $table->foreign('id_carnet')->references('id')->on('students');   
            $table->foreign('id_tema')->references('id')->on('themes');       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
