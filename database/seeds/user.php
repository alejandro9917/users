<?php

use Illuminate\Database\Seeder;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert
        ([
            'str_nombre' => 'Miguel',
            'str_apellido' => 'Melendez',
            'nbr_usuario' => 'MM180363',
            'password' => bcrypt('mm180363')
        ]);
    }
}
