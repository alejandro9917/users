<?php

namespace App\Http\Controllers;

use App\Theme;
use App\Vote;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $temas = Theme::get();

        return view('historial')->with('temas', $temas);
    }

    public function crear()
    {
        return view('generar');
    }

    public function agregar(Request $request)
    {
        Theme::create([
            'str_tema' => $request->str_tema,
            'str_descripcion' => $request->str_descripcion,
            'estado' => true
        ]);

        return redirect()->route('dash.historial');
    }

    public function graficas($id)
    {
        $tema = Theme::where('id', $id)->first();

        $datos = [
            count(Vote::where([['id_tema', $id], ['num_valor', 1]])->get()),
            count(Vote::where([['id_tema', $id], ['num_valor', 2]])->get()),
            count(Vote::where([['id_tema', $id], ['num_valor', 0]])->get())
        ];

        //$datos = Vote::where([['id_tema', $id], ['num_valor', 1]])->get();
        //dd($datos);
        return view('grafica', ['datos' => $datos])->with('tema', $tema);
    }
}
